//
// Created by tibor on 24. 09. 2021..
//
#include "pathfollow_server/subpath.h"
#include <iomanip>

Subpath::Subpath(const std::vector<agv_msgs::Segment>& segments, const VehicleParameters& vehicleParameters)
    : segments_(segments)
{
  auto curveListFromSegment = [&](const agv_msgs::Segment& segment)
  {
    auto curveVector = std::vector<std::shared_ptr<Bezier::Curve>>();
    for (const agv_msgs::Part& part : segment.parts)
    {
      std::vector<Bezier::Point> bezier_points;
      for (const geometry_msgs::Point& point : part.points)
      {
        bezier_points.emplace_back(point.x, point.y);
      }
      curveVector.push_back(std::make_shared<Bezier::Curve>(Bezier::Curve(bezier_points)));
    }
    return curveVector;
  };

  auto poseToBezierPoint = [](const geometry_msgs::Pose& pose)
  { return Bezier::Point(pose.position.x, pose.position.y); };

  // generate polycurve
  for (const auto& segment : segments)
  {
    for (auto curve_sp : curveListFromSegment(segment))
    {
      polyCurve_.insertBack(curve_sp);
    }
  }

  direction_ = segments.front().direction;

  float last_speed{};
  double creep_t = polyCurve_.iterateByLength(0.0, polyCurve_.length() - vehicleParameters.creep_distance);

  // velocity limit profile is calculated from the back of the subpath
  for (int segment_idx = segments_.size() - 1; segment_idx >= 0; --segment_idx)
  {
    // calculate the curve parameter t of first and last point of the segment
    double start = polyCurve_.projectPoint(poseToBezierPoint(segments_[segment_idx].begin.pose));

    double end = polyCurve_.projectPoint(poseToBezierPoint(segments_[segment_idx].end.pose));

    float segment_max_speed = segments_[segment_idx].velocity_limit;

    std::vector<Bezier::Point> segment_points =
        polyCurve_.subPolyCurve(polyCurve_.curveIdx(start),
                                polyCurve_.curveIdx(end)).polyline(1.01, 0.01);

    // calculate maximum velocity using curvature of the segment curve
    for (const Bezier::Point& point : segment_points)
    {
      segment_max_speed = std::min(
          segment_max_speed,
          static_cast<float>(std::sqrt((0.11 * 9.81) / polyCurve_.curvatureAt(polyCurve_.projectPoint(point)))));
    }

    if (start > creep_t && end > creep_t)
    {
      segment_max_speed = std::min(segment_max_speed, vehicleParameters.creep_speed);
    }
    else if (start < creep_t && end > creep_t)
    {
      end = creep_t;
      last_speed = std::min(segment_max_speed, vehicleParameters.creep_speed);
      // add end point to limit
      speedLimit_.emplace_back(polyCurve_.size(), last_speed);
      speedLimit_.emplace_back(creep_t, last_speed);
    }
    // flag used to determine if the end point of the segment must be added to the
    // limit
    bool add_end = true;

    // if current segment maximum velocity is higher than next segment maximum
    // velocity, the vehicle must decelerate
    if (segment_max_speed > last_speed)
    {
      // calculate the distance needed to decelerate from current segment
      // maximum velocity to next segment maximum velocity
      double deacc_distance =
          (std::pow(segment_max_speed, 2.0) - std::pow(last_speed, 2.0)) / (2 * vehicleParameters.max_deceleration);
      // if that distance is larger than segment length, than calculate the new
      // maximum velocity
      //  with which the vehicle can safely decelerate to next segment maximum
      //  velocity on current segment.
      // NOTE: that velocity will surely be smaller than maximum velocity of
      // current segment
      double segment_length = polyCurve_.length(start, end);
      if (deacc_distance > segment_length)
      {
        segment_max_speed = static_cast<float>(
            std::sqrt(2 * vehicleParameters.max_deceleration * segment_length + std::pow(last_speed, 2)));
        // there isn't enough distance for vehicle to decelerate - there is no
        // point to add to velocity limit
        add_end = false;
      }
      else
      {
        // there is enough distance for vehicle to decelerate - calculate the
        // curve parameter of the point where the vehicle must begin to
        // decelerate
        end = polyCurve_.iterateByLength(start, segment_length - deacc_distance);
      }
    }

    // add point where the vehicle must begin to decelerate if found
    if (add_end)
      speedLimit_.emplace_back(end, segment_max_speed);
    // add starting point of limit
    speedLimit_.emplace_back(start, segment_max_speed);

    last_speed = segment_max_speed;
  }
  speedLimit_.emplace_back(0.0, 0.0);

  std::reverse(speedLimit_.begin(), speedLimit_.end());

  // sample the calculated limit
  std::vector<std::pair<double, double>> newLimit;

  std::function<void(const std::pair<double, double>&, const std::pair<double, double>&)> sampleLimit =
      [&](const std::pair<double, double>& first, const std::pair<double, double>& second)
  {
    // if the second velocity is higher(vehicle must accelerate) or they are
    // already close enough --> no sampling is needed
    if (second.second >= first.second || std::abs(first.second - second.second) < 1e-5 || first.first >= second.first)
      return;

    double t = (first.first + second.first) / 2;

    double deceleration = t > creep_t ? (std::pow(vehicleParameters.creep_speed, 2.0) / (2*vehicleParameters.creep_distance)) : vehicleParameters.max_deceleration;

    // velocity calculated using linear interpolation
    double v_c = ((t - first.first) / (second.first - first.first)) * (second.second - first.second) + first.second;

    // velocity calculated using kinematic equations
    double v_s = std::max(0.0, std::sqrt(std::pow(first.second, 2) -
                                         2 * deceleration * polyCurve_.length(first.first, t)));

    // if they are close enough, return
    if (std::abs(v_s - v_c) < 1e-5)
      return;

    std::pair<double, double> newPoint(t, v_s);
    sampleLimit(first, newPoint);
    newLimit.push_back(newPoint);
    sampleLimit(newPoint, second);
  };

  newLimit.push_back(speedLimit_.front());
  for (int limit_point_idx = 1; limit_point_idx < speedLimit_.size(); ++limit_point_idx)
  {
    sampleLimit(speedLimit_[limit_point_idx - 1], speedLimit_[limit_point_idx]);
    newLimit.push_back(speedLimit_[limit_point_idx]);
  }
  speedLimit_ = std::move(newLimit);

}

double Subpath::getLimit(const geometry_msgs::Pose& pose) const
{

  double t = polyCurve_.projectPoint(Bezier::Point(pose.position.x, pose.position.y));
  if (t>=polyCurve_.size()) return 0.0;

  // Pair which has the higher curve parameter that t
  auto up = std::upper_bound(speedLimit_.begin(), speedLimit_.end(), t,
                             [](const double& val, const std::pair<double, double>& pair) { return val < pair.first; });
  auto low = up - 1;

  // reached the ond or no deacc needed --> return lower velocity as limit
  if (up == speedLimit_.end() || low->second == up->second)
    return low->second;

  if (std::abs(low->first - up->first) < 1e-6)
    return up->second;

  // else calculate limit using linear interpolation
  return ((t - low->first) / (up->first - low->first)) * (up->second - low->second) + low->second;
}

const Bezier::PolyCurve& Subpath::getCurve() const { return polyCurve_; }

int Subpath::getDirection() const { return direction_; }

std::vector<agv_msgs::Segment> Subpath::getSegments() const { return segments_; }
