//
// Created by tibor on 24. 09. 2021..
//

#include "pathfollow_server/pure_pursuit_vehicle.h"

PurePursuitVehicle::PurePursuitVehicle(const VehicleParameters& vehicleParameters) : params_(vehicleParameters)
{
  lastVelocity_ = 0.0;
}

double PurePursuitVehicle::angularVelocity(geometry_msgs::Pose lookaheadPose) const
{
  double x = lookaheadPose.position.x;
  double y = lookaheadPose.position.y;

  return (2 * lastVelocity_ * y) / (pow(y, 2) + pow(x, 2));
}

double PurePursuitVehicle::velocity(double limit)
{
  double v{};

  // if you haven't reached the limit yet --> accelerate, else follow it
  v = std::min({static_cast<double>(params_.max_linear_velocity), limit,
                lastVelocity_ + params_.max_acceleration * params_.time_step});

  // if the last_speed is larger than max_speed even when deceleration is
  // applied --> decelerate
  v = std::max(v, lastVelocity_ - params_.max_deceleration * params_.time_step);

  lastVelocity_ = v;

  return v;
}