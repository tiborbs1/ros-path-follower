//
// Created by tibor on 24. 09. 2021..
//

#include "pathfollow_server/path.h"

Path::Path(const agv_msgs::Path& path, const VehicleParameters& vehicleParameters)
{
  subpaths_ = Path::createSubpaths(path, vehicleParameters);

  completed_length_ = 0.0, current_length_ = 0.0, total_length_ = 0.0;
  done_ = false;

  for (const Subpath& subpath : subpaths_)
  {
    total_length_ += subpath.getCurve().length();
  }
}

std::pair<geometry_msgs::Pose, geometry_msgs::Pose> Path::getProjectedAndFollowPose(geometry_msgs::Pose pose,
                                                                                    double distance, double speed)
{
  auto bezierPointToPose = [&](const Bezier::Point& point){
    const Bezier::PolyCurve& subpath_curve = subpaths_.front().getCurve();

    Bezier::Vector tangent = subpath_curve.tangentAt(subpath_curve.projectPoint(point));

    Eigen::Vector3d p1(1, 0, 0), p2(tangent.x(), tangent.y(), 0);
    Eigen::Quaterniond q_e;

    q_e.setFromTwoVectors(p1, p2);

    geometry_msgs::Pose pose;
    pose.position.x = point.x();
    pose.position.y = point.y();
    pose.orientation.x = q_e.x();
    pose.orientation.y = q_e.y();
    pose.orientation.z = q_e.z();
    pose.orientation.w = q_e.w();

    return pose;
  };

  Bezier::Point position_bezier = Bezier::Point(pose.position.x, pose.position.y);

  Bezier::PolyCurve subpath_curve = subpaths_.front().getCurve();

  double agv_t = subpath_curve.projectPoint(position_bezier);
  double subpath_size = subpath_curve.size();
  double remaining_distance = subpath_curve.length(agv_t, subpath_size);

  // switch subpaths
  if (remaining_distance < 1e-3 and speed <= 0.0)
  {
    // end of subpath
    if (subpaths_.size() > 1)
    {
      completed_length_ += subpath_curve.length();
      subpaths_.pop_front();
      subpath_curve = subpaths_.front().getCurve();
      agv_t = subpath_curve.projectPoint(position_bezier);
      subpath_size = subpath_curve.size();
      remaining_distance = subpath_curve.length(agv_t, subpath_size);
    }
    // end of path
    else if (subpaths_.size() == 1)
    {
      done_ = true;
      agv_t = subpath_curve.size();
    }
  }

  geometry_msgs::Pose projected_pose = bezierPointToPose(subpath_curve.valueAt(agv_t));

  //return pure projection
  if(distance == 0.0){
    return std::make_pair(projected_pose, projected_pose);
  }
  //return follow point
  else {
    double fp_t = subpath_curve.iterateByLength(agv_t, distance);

    Bezier::Point follow_point_bezier = subpath_curve.valueAt(fp_t);

    if(remaining_distance < distance){
      follow_point_bezier += (distance - remaining_distance) * subpath_curve.tangentAt(subpath_curve.size());
    }

    return std::make_pair(projected_pose, bezierPointToPose(follow_point_bezier));
  }
}

double Path::getLimit(geometry_msgs::Pose pose) const { return subpaths_.front().getLimit(pose); }

bool Path::isDone() const { return done_; }

float Path::getPercentage() const { return static_cast<float>(current_length_ / total_length_); }

int Path::getDirection() const { return subpaths_.front().getDirection(); }

std::deque<Subpath> Path::createSubpaths(const agv_msgs::Path& path, const VehicleParameters& vehicleParameters)
{
    std::deque<Subpath> subpaths;
    std::vector<agv_msgs::Segment> segments;

    int direction = path.segments.front().direction;
    int current_segment_idx = 0;

    while (current_segment_idx < path.segments.size())
    {
      //while current segments is of same direction as last, add it to the segments vector
      if (path.segments[current_segment_idx].direction == direction)
      {
        segments.push_back(path.segments[current_segment_idx++]);
      }
      //when the direction changes, generate subpath and put it to the subpaths vector
      else {
        subpaths.emplace_back(segments, vehicleParameters);
        direction = path.segments[current_segment_idx].direction;
        segments.clear();
      }
    }
    //generate and append last subpath
    subpaths.emplace_back(segments, vehicleParameters);
    return subpaths;
}

Path Path::extendPath(Path path, agv_msgs::Path rosPath, const VehicleParameters& vehicleParameters)
{
  std::deque<Subpath> newSubpaths = Path::createSubpaths(rosPath, vehicleParameters);

  // if the first new subpath is of same direction as the last old subpath then
  // -->
  if (newSubpaths.front().getDirection() == path.subpaths_.back().getDirection())
  {
    std::vector<agv_msgs::Segment> segments = path.subpaths_.back().getSegments();

    //append new segments to last subpath segments
    for (const agv_msgs::Segment& segment : newSubpaths.front().getSegments())
    {
      segments.push_back(segment);
    }
    //replace last subpath with the new subpath
    path.subpaths_.back() = Subpath(segments, vehicleParameters);
    // delete first new subpath because the last old subpath was extended
    newSubpaths.pop_front();
  }
  // insert new subpaths to old subpaths
  if (not newSubpaths.empty())
    path.subpaths_.insert(path.subpaths_.end(), newSubpaths.begin(), newSubpaths.end());

  // add new path length to old path length
  for (const Subpath& subpath : newSubpaths)
  {
    path.total_length_ += subpath.getCurve().length();
  }
  return path;
  }
bool Path::empty() { return subpaths_.empty(); }
