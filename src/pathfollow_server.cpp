//
// Created by tibor on 25. 06. 2021..
//

#include "pathfollow_server/pathfollow_server.h"

PathFollowServer::PathFollowServer(const std::string& name)
    : nh_("path_follower"), as_(nh_, name, false), action_name_(name), tf2_listener_(tf2_buffer_)
{

  // necessary publishers
  cmdVelPublisher_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  pathPublisher_ = nh_.advertise<agv_msgs::PathStamped>("path", 1);

  // helpful publishers
  maxVelocityPublisher_ = nh_.advertise<std_msgs::Float32>("path_velocity_limit", 1);
  followPointPublisher_ = nh_.advertise<visualization_msgs::Marker>("follow_point", 1);
  projectedPointPublisher_ = nh_.advertise<visualization_msgs::Marker>("projected_point", 1);

  // necessary subscribers
  odomSubscriber_ = nh_.subscribe("odom", 1000, &PathFollowServer::odomCB, this);
  velocityLimitSubscriber_ = nh_.subscribe("set_velocity_limit", 1000, &PathFollowServer::velocityLimitCB, this);
  appendPathSubscriber_ = nh_.subscribe("append_path", 1000, &PathFollowServer::appendPath, this);

  userVelocityLimit_ = 0.0;

  bool params_success = true;
  params_success &= nh_.getParam("global_frame", global_frame_id_);
  params_success &= nh_.getParam("robot_frame", robot_frame_id_);

  params_success &= nh_.getParam("min_lookahead_distance", ppParams_.min_lookahead_distance);
  params_success &= nh_.getParam("max_lookahead_distance", ppParams_.max_lookahead_distance);
  params_success &= nh_.getParam("min_lookahead_speed", ppParams_.min_lookahead_speed);
  params_success &= nh_.getParam("max_lookahead_speed", ppParams_.max_lookahead_speed);

  params_success &= nh_.getParam("creep_speed", vehicleParams_.creep_speed);
  params_success &= nh_.getParam("creep_distance", vehicleParams_.creep_distance);
  params_success &= nh_.getParam("max_linear_velocity", vehicleParams_.max_linear_velocity);
  params_success &= nh_.getParam("acceleration", vehicleParams_.max_acceleration);
  params_success &= nh_.getParam("deceleration", vehicleParams_.max_deceleration);
  params_success &= nh_.getParam("time_step", vehicleParams_.time_step);

  if (!params_success)
    ROS_FATAL("Unable to load parameters needed for pure pursuit algorithm!");
  vehicle_ = PurePursuitVehicle(vehicleParams_);
  goalPreempted_ = false;
  lastDirection_ = 0;

  odomLock_ = std::unique_lock(odomMutex_, std::defer_lock);

  as_.registerPreemptCallback(boost::bind(&PathFollowServer::preemptCB, this)); // NOLINT(modernize-avoid-bind)
  as_.registerGoalCallback(boost::bind(&PathFollowServer::goalCB, this));       // NOLINT(modernize-avoid-bind)
  as_.start();
}

void PathFollowServer::preemptCB()
{
  // if goal is preempted --> vehicle begins to decelerate
  userVelocityLimit_ = 0.0;
  ROS_INFO("%s: Preempted", action_name_.c_str());
  as_.setPreempted();
  goalPreempted_ = true;
}

void PathFollowServer::goalCB()
{
  goal_ = as_.acceptNewGoal()->path_stamped;
  if (goal_.path.segments.empty())
  {
    ROS_ERROR("Received empty goal!");
  }
  else if (global_frame_id_ != goal_.header.frame_id)
  {
    ROS_ERROR("Received goal which is not in the same frame as the global frame of the robot!");
  }
  else
  {
    std::thread(
        [&]()
        {
          goalPreempted_ = false;
          path_ = Path(goal_.path, vehicleParams_);
          userVelocityLimit_ = vehicleParams_.max_linear_velocity;
          pathPublisher_.publish(goal_);
        })
        .detach();
  }
}

void PathFollowServer::odomCB(const nav_msgs::Odometry& odom)
{

  odomLock_.lock();
  auto poseToMarker = [&](const geometry_msgs::Pose& pose, float r, float g, float b)
  {
    visualization_msgs::Marker marker;
    marker.header.frame_id = global_frame_id_;
    marker.header.stamp = ros::Time();
    marker.id = 0;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose = pose;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = r;
    marker.color.g = g;
    marker.color.b = b;
    return marker;
  };

  auto waitForTransform = [&](const std::string& source_frame_id, const std::string& dest_frame_id)
  {
    bool transform_found = false;
    while (!transform_found)
    {
      try
      {
        tf_ = tf2_buffer_.lookupTransform(dest_frame_id, source_frame_id, ros::Time(0));
        transform_found = true;
      }
      catch (const tf2::TransformException& exception)
      {
        ROS_ERROR("Transform between %s and %s not found!", robot_frame_id_.c_str(), global_frame_id_.c_str());
      }
    }
  };

  auto calculateLookaheadDistance = [&](double current_speed){
    if(current_speed < ppParams_.min_lookahead_speed){
      return ppParams_.min_lookahead_distance;
    }
    else if(current_speed > ppParams_.max_lookahead_speed){
      return ppParams_.max_lookahead_distance;
    }
    else {
      return ((current_speed - ppParams_.min_lookahead_speed) / (ppParams_.max_lookahead_speed - ppParams_.min_lookahead_speed)) *
                 (ppParams_.max_lookahead_distance - ppParams_.min_lookahead_distance) + ppParams_.min_lookahead_distance;
    }
  };

  geometry_msgs::Twist msg;
  geometry_msgs::Pose current_pose;
  current_pose.orientation.w = 1.0;

  waitForTransform(robot_frame_id_, global_frame_id_);

  // calculate current pose in global frame
  tf2::doTransform(current_pose, current_pose, tf_);

  // if a goal is active --> publish feedback
  if (as_.isActive() && !path_.empty() || goalPreempted_)
  {

    auto path_limit = static_cast<float>(path_.getLimit(current_pose));
    double vehicle_speed = vehicle_.velocity(std::min(userVelocityLimit_, path_limit));

    double lookahead_distance = calculateLookaheadDistance(vehicle_speed);

    auto pose_pair = path_.getProjectedAndFollowPose(current_pose, lookahead_distance, vehicle_speed);

    lastLookaheadPose_ = pose_pair.second;
    lastDirection_ = path_.getDirection();

    followPointPublisher_.publish(poseToMarker(lastLookaheadPose_, 0, 1, 0));

    waitForTransform(global_frame_id_, robot_frame_id_);

    tf2::doTransform(lastLookaheadPose_, lastLookaheadPose_, tf_);

    std_msgs::Float32 velocity_limit_msg;
    velocity_limit_msg.data = static_cast<float>(path_.getDirection()) * path_limit;

    maxVelocityPublisher_.publish(velocity_limit_msg);

    geometry_msgs::Pose projected_pose = pose_pair.first;

    projectedPointPublisher_.publish(poseToMarker(projected_pose, 1, 0, 0));

    geometry_msgs::Pose base_error_pose = current_pose;

    waitForTransform(robot_frame_id_, global_frame_id_);

    tf2::doTransform(current_pose, base_error_pose, tf_);

    if (!goalPreempted_)
    {
      feedback_.base_pose.pose = current_pose;
      feedback_.base_error.pose = base_error_pose;
      feedback_.percent_complete = path_.getPercentage();
      as_.publishFeedback(feedback_);
    }

    // if done with goal --> stop and publish success message
    if (path_.isDone())
    {
      userVelocityLimit_ = 0.0;
      result_.success = true;
      as_.setSucceeded(result_, "Path following successful! :)");
      goalPreempted_ = false;
      lastDirection_ = 0;
    }
    msg.linear.x = lastDirection_ * vehicle_speed;
    msg.angular.z = lastDirection_ * vehicle_.angularVelocity(lastLookaheadPose_);
  }
  else
  {
    msg.linear.x = lastDirection_ * vehicle_.velocity(0.0);
    msg.angular.z = 0.0;
  }
  cmdVelPublisher_.publish(msg);
  odomLock_.unlock();
}

void PathFollowServer::velocityLimitCB(const pathfollow_server::SpeedLimitStamped& speedLimitMsg)
{
  if (speedLimitMsg.velocity_limit < 0.0)
  {
    ROS_WARN("Velocity limit must be greater or equal zero! Velocity limit NOT set.");
    return;
  }

  speedLimitMap_[speedLimitMsg.sender_id] = speedLimitMsg.velocity_limit;
  userVelocityLimit_ =
      std::min_element(speedLimitMap_.begin(), speedLimitMap_.end(),
                       [](const std::pair<std::string, float>& first, const std::pair<std::string, float>& second)
                       { return first.second < second.second; })->second;
}

void PathFollowServer::appendPath(const agv_msgs::PathStamped& path)
{
  if (path.path.segments.empty())
  {
    ROS_WARN("Received empty path. Cannot append path!");
  }
  else if (global_frame_id_ != path.header.frame_id)
  {
    ROS_WARN("Received path which is not in the same frame as the global frame of the robot. Cannot append path!");
  }
  else if (!as_.isActive())
  {
    ROS_WARN("No active goals. Cannot append path!");
  }
  else
  {
    pathsToAppend_.push_back(path.path);
    if (pathsToAppend_.size() == 1)
    {
      std::thread(
          [&]()
          {
            std::unique_lock<std::mutex> odomLock(odomMutex_, std::defer_lock);
            while (!pathsToAppend_.empty())
            {
              Path newPath = Path::extendPath(path_, pathsToAppend_.front(), vehicleParams_);
              pathsToAppend_.pop_front();
              odomLock.lock();
              path_ = newPath;
              odomLock.unlock();
            }
          })
          .detach();
    }
    goal_.path.segments.insert(goal_.path.segments.end(), path.path.segments.begin(), path.path.segments.end());
    goal_.header.seq = path.header.seq;
    goal_.header.stamp = ros::Time();
    goal_.header.frame_id = global_frame_id_;
    pathPublisher_.publish(goal_);
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pathfollow_server");

  PathFollowServer followServer("pathfollow_server");

  ros::spin();

  return 0;
}