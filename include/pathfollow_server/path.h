//
// Created by tibor on 24. 09. 2021..
//

/** Path: This header file contains all the structs necessary for representing the path.
 * @file path.h
 * @author Tibor Bataljak Savić
 * @version 1.0
 */

#ifndef SRC_PATH_H
#define SRC_PATH_H
#include "subpath.h"
#include <agv_msgs/Path.h>
#include <mutex>

/**
 * Implementation of a path.
 *
 */
class Path
{
private:
  std::deque<Subpath> subpaths_;
  double completed_length_{}, current_length_{}, total_length_{};
  bool done_{false};

  /** Creates subpaths from received path.
   *
   * @param path ROS agv_msgs::Path path
   * @param vehicleParameters the parameters of the vehicle
   * @return deque of subpaths
   */
  static std::deque<Subpath> createSubpaths(const agv_msgs::Path& path, const VehicleParameters& vehicleParameters);

public:
  /**
   * Default constructor for a path. Constructs an empty path.
   *
   */
  Path() = default;

  /** Constructor.
   *
   * @param path agv_msgs path ROS message from which the path is generated
   * @param vehicleParameters the parameters of the vehicle
   */
  Path(const agv_msgs::Path& path, const VehicleParameters& vehicleParameters);

  /** Default destructor.
   *
   */
  ~Path() = default;

  /**
   * Returns the projected and the follow pose which is distance away from projected robot pose.
   *
   * @param pose current robot pose
   * @param distance lookahead distance
   * @param speed current robot speed
   * @return pair consisting of projected and follow pose
   */
  std::pair<geometry_msgs::Pose, geometry_msgs::Pose> getProjectedAndFollowPose(geometry_msgs::Pose pose,
                                                                                double distance = 0.0, double speed=0.0);
  /**
   * Calculates current path velocity limit.
   *
   * @param pose current robot pose
   * @return current path velocity limit
   */

  double getLimit(geometry_msgs::Pose pose) const;

  /** Returns a boolean flag which contains info on path completion.
   *
   * @return true if path is done, false otherwise
   */
  bool isDone() const;

  /** Returns the percentage of path completed.
   *
   * @return path completion percentage
   */
  float getPercentage() const;

  /** Returns current direction.
   *
   * @return current travel direction of robot
   */
  int getDirection() const;

  /** Returns info on path emptiness.
   *
   * @return true if path is empty, false otherwise
   */
  bool empty();

  /** Extends the current path with the new received path.
   *
   * @param path current path
   * @param rosPath received agv_msgs ROS path
   * @param vehicleParameters the parameters of the vehicle
   * @return extended path
   */
  static Path extendPath(Path path, agv_msgs::Path rosPath, const VehicleParameters& vehicleParameters);
};

#endif // SRC_PATH_H
