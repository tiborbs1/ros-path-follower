//
// Created by tibor on 07. 09. 2021..
//
/** Vehicle: This header file contains all the structs necessary for
 * representing an autonomous ground vehicle which uses the PurePursuit
 * algorithm to follow a set path.
 * @file pure_pursuit_vehicle.h
 * @author Tibor Bataljak Savić
 * @version 1.0
 */

#ifndef SRC_PURE_PURSUIT_VEHICLE_H
#define SRC_PURE_PURSUIT_VEHICLE_H
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>

/**
 * Vehicle parameters needed for the use in PurePursuit algorithm.
 */
struct VehicleParameters
{
  float max_linear_velocity;
  double max_acceleration;
  double max_deceleration;
  double time_step;
  float creep_speed;
  double creep_distance;

};

/**
 * Implementation of a vehicle which uses the pure pursuit algorithm to follow a path.
 *
 */

class PurePursuitVehicle
{
public:
  /** Constructor.
   *
   * @param vehicleParameters the parameters of the vehicle
   */
  explicit PurePursuitVehicle(const VehicleParameters& vehicleParameters = VehicleParameters());
  /** Default destructor.
   *
   */
  ~PurePursuitVehicle() = default;

  /** Calculates the angular velocity using the given lookahead pose.
   *
   * @param lookaheadPose lookahead pose
   * @return angular velocity of the agv
   */
  double angularVelocity(geometry_msgs::Pose lookaheadPose) const;

  /** Calculates the velocity. When calculating the velocity, path and vehicle limit is taken into consideration.
   *
   * @param limit current velocity limit
   * @return velocity of the robot
   */
  double velocity(double limit);

private:
  VehicleParameters params_;
  double lastVelocity_;
};
#endif // SRC_PURE_PURSUIT_VEHICLE_H
