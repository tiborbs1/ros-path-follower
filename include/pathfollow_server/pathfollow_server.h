//
// Created by tibor on 07. 07. 2021..
//
/** Pathfollow Server: contains the path-following action server implementation.
 * @file pathfollow_server.h
 * @author Tibor Bataljak Savić
 * @version 1.0
 */
#ifndef SRC_PATHFOLLOW_SERVER_H
#define SRC_PATHFOLLOW_SERVER_H
#include "path.h"
#include <actionlib/server/simple_action_server.h>
#include <agv_msgs/FollowPathAction.h>
#include <agv_msgs/FollowPathActionFeedback.h>
#include <agv_msgs/FollowPathResult.h>
#include <cmath>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Odometry.h>
#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/String.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <visualization_msgs/Marker.h>
#include <thread>

#include "pathfollow_server/SpeedLimitStamped.h"

/**
 * Implementation of a ROS action server for path-following.
 */
class PathFollowServer
{
protected:
  ros::NodeHandle nh_;
  ros::Publisher cmdVelPublisher_, pathPublisher_, maxVelocityPublisher_, projectedPointPublisher_,
      followPointPublisher_;
  ros::Subscriber odomSubscriber_, appendPathSubscriber_, velocityLimitSubscriber_;

  geometry_msgs::TransformStamped tf_;
  geometry_msgs::Pose lastLookaheadPose_;

  tf2_ros::Buffer tf2_buffer_;
  tf2_ros::TransformListener tf2_listener_;

  actionlib::SimpleActionServer<agv_msgs::FollowPathAction> as_;

  std::string action_name_, global_frame_id_, robot_frame_id_;
  std::map<std::string, float> speedLimitMap_;
  std::mutex odomMutex_;
  std::unique_lock<std::mutex> odomLock_;
  std::deque<agv_msgs::Path> pathsToAppend_;

  agv_msgs::FollowPathFeedback feedback_;
  agv_msgs::FollowPathResult result_;
  agv_msgs::PathStamped goal_;

  Path path_;

  PurePursuitParameters ppParams_;
  VehicleParameters vehicleParams_;
  PurePursuitVehicle vehicle_;

  float userVelocityLimit_;
  int lastDirection_;
  bool goalPreempted_;

  /** Sets the velocity limit for the user specified in the SpeedLimitStamped message.
   *
   * @param speedLimitMsg SpeedLimitStamped message
   */
  void velocityLimitCB(const pathfollow_server::SpeedLimitStamped& speedLimitMsg);

  /** Appends received path.
   *
   * @param path agv_msgs::PathStamped ROS message
   */
  void appendPath(const agv_msgs::PathStamped& path);
public:
  /**
   * Constructor.
   *
   * @param name server name
   * @param ppParams pure pursuit parameters
   * @param vehicleParameters vehicle parameters
   */
  explicit PathFollowServer(const std::string& name);

  /**
   * Destructor.
   *
   */
  ~PathFollowServer() = default;

  /**
   * Preempt callback.
   */
  void preemptCB();

  /**
   * New goal callback.
   */
  void goalCB();

  /**
   * Odometry callback.
   *
   * @param odom odometry message
   */
  void odomCB(const nav_msgs::Odometry& odom);
};
#endif // SRC_PATHFOLLOW_SERVER_H
