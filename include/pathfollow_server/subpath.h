//
// Created by tibor on 26. 08. 2021..
//

/** Subpath: This header file contains all the structs necessary for
 * representing the subpath.
 * @file subpath.h
 * @author Tibor Bataljak Savić
 * @version 1.0
 */

#ifndef SRC_SUBPATH_H
#define SRC_SUBPATH_H
#include "pure_pursuit_vehicle.h"
#include <Bezier/bezier.h>
#include <Bezier/polycurve.h>
#include <agv_msgs/Segment.h>

/**
 * Parameters needed for the pure pursuit algorithm.
 */
struct PurePursuitParameters
{
  double min_lookahead_distance;
  double max_lookahead_distance;
  double min_lookahead_speed;
  double max_lookahead_speed;
};

/**
 * Representation of a subpath. A subpath is a part of the path from one inverse
 * point to the next. An inverse point is a point where the vehicle changes the
 * direction of travel. Subpaths are represented as Bezier curves. To do this,
 * the Bezier library is used.
 *
 *  @see [Bezier](https://github.com/romb-technologies/Bezier)
 */
class Subpath
{
private:
  std::vector<agv_msgs::Segment> segments_;
  Bezier::PolyCurve polyCurve_;
  std::vector<std::pair<double, double>> speedLimit_;
  int direction_;

public:
  /** Constructor.
   *
   * @param segments vector of agv_msgs segments on the path
   * @param vehicleParameters parameters of the vehicle
   */
  Subpath(const std::vector<agv_msgs::Segment>& segments, const VehicleParameters& vehicleParameters);

  /** Return the Bezier curve representing the subpath.
   *
   * @return polycurve representation of a subpath
   */
  const Bezier::PolyCurve& getCurve() const;

  /**
    * Calculates current subpath velocity limit.
    *
    * @param pose current robot pose
    * @return current subpath velocity limit
    */
  double getLimit(const geometry_msgs::Pose& pose) const;

  /** Returns subpath direction.
   *
   * @return travel direction of robot when on this subpath
   */
  int getDirection() const;

  /** Returns segments from which the subpath was generated.
   *
   * @return vetor of agv_msgs segments
   */
  std::vector<agv_msgs::Segment> getSegments() const;
};
#endif // SRC_SUBPATH_H
