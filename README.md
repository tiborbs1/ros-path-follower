# Description

This repository contains an implementation of a ROS path-follower which uses the pure-pursuit algorithm to follow the path. 
Paths are represented using Bezier curves. For Bezier curve generation, the [Bezier](https://github.com/romb-technologies/Bezier) library is used. 
For more details about the pure-pursuit algorithm, please check out the original paper on pure pursuit at the following [link](https://www.ri.cmu.edu/pub_files/pub3/coulter_r_craig_1992_1/coulter_r_craig_1992_1.pdf).   

# Dependencies 

* [Bezier](https://github.com/romb-technologies/Bezier)
* [agv_msgs](https://gitlab.com/romb-technologies/agv_msgs.git)


# Get started

1. Install the listed dependencies.  
 
3. Clone the repository into the `src` subdirectory of your catkin workspace.
```bash 
git clone https://tiborbs1@bitbucket.org/tiborbs1/ros-path-follower.git
```

4. Position yourself into your catkin workspace and rebuild it.
```bash
cd ~/catkin_ws
catkin_make
```

5. Launch `roscore`.
```bash 
roscore
```

6. Launch your simulation or set up your robot.

7. Launch the path follower node.
```bash
roslaunch ROS_NAMESPACE=<your_robot_ns> pathfollow_server path_following.launch 
```

# Notice 

This project was a part of my student intership at [Romb Technologies](https://romb-technologies.hr/). 
It was tested on the Pioneer 3-DX robot, but should be used with caution. I haven't tested it thoroughly, 
as I have conceived this project to only be a showcase of the pure-pursuit algorithm and not to be used in serious projects. 
However, I will try to test it more and further develop and maintain it as much as I can. 